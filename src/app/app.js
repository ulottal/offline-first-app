import faker from 'faker';

import { set, get, keys, del } from 'idb-keyval';

import './boite-outils.css'

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('../service-worker.js')
  .then(function(registration) {
    console.log('Registration successful, scope is:', registration.scope);
  })
  .catch(function(error) {
    console.log('Service worker registration failed, error:', error);
  });
}


let state = {
  isOnline: navigator.onLine,
  list: []
}


const render = () => {

  // View
  document.body.innerHTML = /*html*/`
    <main class="main-content" id="main-content" aria-label="Main Content">

      <header class="content-header ">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="content-header-title">
                <h1 class="display-2">Offline-first app</h1>
                <br />
                <p class="lead">Experimenting with IndexedDB & Service workers</p>
              </div>
            </div>
          </div>
        </div>
      </header>

      ${
        state.isOnline
        ? (/*html*/`
          <div class="container">
            <div class="alert alert-success" role="alert">
              <strong>Online!</strong> Connectivity has returned, you are now online.
            </div>
          </div>
        `)
        : (/*html*/`
          <div class="container">
            <div class="alert alert-warning" role="alert">
              <strong>Offline!</strong> Connectivity is lost, you are now offline.
            </div>
          </div>
        `)
      }
    
      <div class="bg-gray-sky lead-container ">
        <div class="section-content-fluid">
          <div class="section-inner section-inner-lg">
          <h2>Total items: <span class="badge">${state.list.length}</span></h2>
          <button class="btn btn-primary generate-btn">Generate 1 item</button>
        </div>
      </div>
    
      <div class="region-content">
        <div class="container ">
          <div class="row">
            <table class="table">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
                ${state.list.map((item, i) => /*html*/`
                  <tr data-id="${item.id}">
                    <td>${i + 1}</td>
                    <td>${item.name}</td>
                    <td>${item.email}</td>
                    <td><button class="delete-btn btn btn-link">Delete</button></td>
                  </tr>
                `).join('')}
            </table>
            <h2>Total items: <span class="badge">${state.list.length}</span></h2>
          </div>
        </div>
      </div>
    </main>
  `;



  // =====================
  // Enable view events
  // =====================

  // Add item to indexeddb
  document.querySelector('.generate-btn').onclick = ev => {
    const item = {
      id: faker.random.uuid(),
      name: faker.name.findName(),
      email: faker.internet.email()
    }
    set(item.id, item).then(getItemsAndRender)
  }

  // Delete item form indexedb
  document.querySelectorAll('.delete-btn').forEach(btn => btn.onclick = ev => 
    del(ev.target.parentElement.parentElement.dataset.id).then(getItemsAndRender)
  )
}





// // Connectivity checker
// setInterval(() => {
//   state.isOnline = navigator.onLine
//   render()
// }, 1000)






const getItemsAndRender = () => {
  // Get all indexeddb items
  keys().then(keys => {
    Promise.all(keys.map(key => get(key))).then(allItems => {
      state.list = allItems
      render()
    });
  });
}


getItemsAndRender()